'use strict';
/* global AWS */

var latitude = 0;
var longitude = 0;

AWS.config.update({
  region: 'ap-southeast-2',
  accessKeyId: 'AKIAJ2F7NPQZZGWKFO2Q',
  secretAccessKey: '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'
});

var lambda = new AWS.Lambda();

// function addImage(id, view) {
//
//   var params = {
//     FunctionName: 'onthespotNewsItem',
//     Payload: JSON.stringify({"id": id})
//   };
//
//   lambda.invoke(params, function(err, data) {
//     if (err) {
//       console.log(err, err.stack); // an error occurred
//     } else {
//       //Update lastTimestamp
//       var response = JSON.parse(data.Payload);
//       if (response.thumbnailLink) {
//         if (response.thumbnailLink['large-16x9']) {
//           var img = view.find("img");
//           img.attr('src', response.thumbnailLink['large-16x9']);
//         }
//       }
//     }
//   });
//
// }

function populateView(view) {

  var params = {
    FunctionName: 'onthespotNews',
    Payload: JSON.stringify({"longitude": longitude, "latitude": latitude})
  };

  lambda.invoke(params, function(err, data) {
    if (err) {
      console.log(err, err.stack); // an error occurred
    } else {
      //Update lastTimestamp
      var response = JSON.parse(data.Payload);
      $.each(response.response.resultPacket.results, function() {
        // TODO add back <img src="" class="news-tile_hero-image">
        view.append($('<a href="'+this.liveUrl+'" class="news-tile"><div class="news-tile_hero"><h2 class="news-tile_hero-title">'+this.title+'</h2></div><p class="news-tile_description">'+this.summary+'</p></a>'));
        // var parts = this.indexUrl.split('/');
        // var id = parts[parts.length-1];
        // addImage(id, view);
      });
    }
  });

}

var UIEngine = {
  _elements:{
    splash: $('.splash-screen'),
    windowObj: $(window)
  },

  // private funtions and methods
  updateLocation: function(position) {
    if (latitude === 0 && longitude === 0) {
      latitude = position.coords.latitude;
      longitude = position.coords.longitude;
      populateView($('.view .view_news'));
    }
  },

  getLocation: function() {
    navigator.geolocation.getCurrentPosition(this.updateLocation);
    navigator.geolocation.watchPosition(this.updateLocation);
  },

  init: function() {
    this.CONTENT.ALERT.init();
    this.addListeners();
    this.getLocation();

    //fade out splash splash-screen
    this._elements.splash.delay(1000).fadeOut();
  },

  addListeners: function(){
    //nav list
    this.HEADER._elements.navList.on('click', 'li', function(e){
      UIEngine.HEADER.updateActive($(e.currentTarget));
    });
  },

  HEADER: {
    // private objects
    _elements: {
      headNav: $('.site-nav .navbar'),
      navList: $('.site-nav .navbar-collapse'),
      currentSelected: $('.site-nav .navbar-nav .active'),
      breadcrumb: $('.site-nav .navbar-heading')
    },
    // private fucntions and methods
    _closeNav: function(){
      this._elements.navList.collapse('toggle');
    },

    // public functions and methods
    updateActive: function(targetObj){
      this.updateBreadcrumb(targetObj.find('a').html());
      UIEngine.CONTENT.openView(targetObj.attr('data-view'));
      this._elements.currentSelected.removeClass('active');
      targetObj.addClass('active');
      this._elements.currentSelected = targetObj;
      this._closeNav();
    },
    updateBreadcrumb: function(titleString){
      this._elements.breadcrumb.html(titleString);
    }
  },

  CONTENT: {

    // private objects
    _elements: {
      viewWindow: $('.view'),
      newsView: $('.view .view_news'),
      alertView: $('.view .view_alert'),
      aboutView: $('.view .view_about'),
      currentView: $('.view .activeView')
    },

    // public functions and methods
    openView: function(viewString) {
      this._elements.currentView.removeClass('activeView');
      switch (viewString){
        case 'news':
          this._elements.currentView = this._elements.newsView.addClass('activeView');
          break;
        case 'alert':
          this._elements.currentView = this._elements.alertView.addClass('activeView');
          //UIEngine.CONTENT.ALERT.init();
          break;
        case 'about':
          this._elements.currentView = this._elements.aboutView.addClass('activeView');
          break;
      }
    },
    ALERT:{
      _elements: {
        header: $('.view_alert-header'),
        contentBody: $('.view_alert-body'),
        footer: $('.view_alert-footer')
      },

      init: function(){
        this.resizeBody();
      },
      resizeBody: function(){
        UIEngine.CONTENT._elements.alertView.height(UIEngine._elements.windowObj.height() - 50);
        // this._elements.contentBody.height(
        //   UIEngine._elements.windowObj.height() -
        //   (
        //     50 +
        //     UIEngine.CONTENT.ALERT._elements.header.height() +
        //     UIEngine.CONTENT.ALERT._elements.footer.height()
        //   )
        // );
      }
    }
  }
};

$(document).ready(function() {
  UIEngine.init();
});
